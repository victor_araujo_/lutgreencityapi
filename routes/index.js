var express = require('express');
var router = express.Router();
var Firebase = require('firebase');
/* GET home page. */

var sha1 = require('sha1');

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i -= 1) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

router.get('/', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.json({ a: 1 });
  //res.render('index', { title: 'Express' });
});

router.get('/v1/tips/random', function(req, res, next) {
  var ref = new Firebase("https://lutgreencity.firebaseio.com/tips");
  ref.once("value").then(function(snapshot) {
  	values = snapshot.val();
  	var randomIndex = Math.floor(Math.random() * Object.keys(values).length);
  	var count = 0;
  	for (var key in values) {
  		if (randomIndex == count) {
  			console.log(snapshot.val()[key]);
  			res.json(snapshot.val()[key]);
  		}
  		count++;
  	}
  });
});

router.get('/v1/tips', function(req, res, next) {
  var ref = new Firebase("https://lutgreencity.firebaseio.com/tips");
  ref.once("value").then(function(snapshot) {
	res.json(snapshot.val());
  });
});



router.post('/v1/tips', function(req, res, next) {
  var data = req.body.tip;
  var createdBy = req.body.createdBy;
  var ref = new Firebase("https://lutgreencity.firebaseio.com/tips");
  var newRef = ref.push({
	  createdBy: createdBy,
	  tip: data
	});

  res.json(JSON.stringify({"name": newRef.key()}));
});

router.delete('/v1/tips/:tipid', function(req, res, next) {
  console.log(req.params);
  var id = req.params.tipid;
  var ref = new Firebase("https://lutgreencity.firebaseio.com/tips/"+id);
  ref.remove().then( function() { res.json({"response": "success"}); } );
  
});

router.get('/v1/users/:userid', function(req, res, next) {
  var userid = req.params.userid;
  var ref = new Firebase("https://lutgreencity.firebaseio.com/users/"+userid);
  ref.once("value", function(snapshot) {
  	console.log(snapshot.val());
	res.json(snapshot.val());
  }, function (errorObject) {
	res.json({"response": "Read failed"});
  });
});

router.get('/v1/ranking/:userid',function(req, res, next) {
	console.log(req.params);
	var userid = req.params.userid; 
	var ref = new Firebase("https://lutgreencity.firebaseio.com/users/");
	var response = [];

	var rank = [];
	var rankNumber;
	var count = 1;
	var accumulated = 0;
	var own_consumption = 0;
	ref.orderByChild("energy_consumption").once("value", function(snapshot) {
	  snapshot.forEach(function(obj) {
	  	rank.push(obj.val());
	  	accumulated += obj.val()['energy_consumption'];
	  	if ( (obj.val()['userid']) == userid ) {
	  		rankNumber = count;
	  		own_consumption = obj.val()['energy_consumption'];
	  	}
	  	count++;
	  });
	  var stats = {
	  	average: (accumulated / (count-1)),
	  	ratio:  (own_consumption / (accumulated / (count-1)))
	  };
	  console.log({users: rank, ranking: rankNumber, count: (count-1), statistics: stats});
	  res.json(JSON.stringify( {users: rank, ranking: rankNumber, count: (count-1), statistics: stats} ));	
	});
});


/*

router.get('/v1/ranking/:rankid',function(req, res, next) {
	console.log(req.params);
	var rankid = req.params.rankid; 
	var ref = new Firebase("https://lutgreencity.firebaseio.com/users/");
	ref.orderByChild("energy_consumption").once("value", function(snapshot) {
	  //console.log(snapshot.val());
	  //console.log(snapshot.val().energy_consumption);
	  snapshot.forEach(function(obj) {

	  	console.log("energy following");
	  	console.log(obj.val()['energy_consumption']);
	  })
	  //console.log(snapshot.key() + " was " + snapshot.val() + " kWh");
	});
	res.json({sent: 1});
}); */

router.post('/v1/energy_readings/', function(req, res, next) {
  	var userid = req.body.userid;
  	var date = req.body.data.time.date;
  	var energy = req.body.data.device.reading.energy;
  	var data = req.body.data;
  	var ref = new Firebase("https://lutgreencity.firebaseio.com/users/"+userid+"/energy_readings/"+date);
  	var newRef = ref.set(data);

	var consumption = new Firebase("https://lutgreencity.firebaseio.com/users/"+userid+"/energy_consumption/");
  	consumption.once("value").then(function(snapshot) {
		energy_consumption = parseInt(snapshot.val());
		energy_consumption += parseInt(energy);
		consumption.set(energy_consumption);
		res.json({"response": "success"});
  	});
});

router.get('/v1/users/:userid/energy_readings', function(req, res, next) {
  var userid = req.params.userid;
  console.log(userid);
	res.json({"response": "Read failed"});
});

router.post('/v1/createUser', function(req, res, next) {
	console.log(req.body);
	var userid = req.body.email;
	var username = req.body.username;
	var material  = req.body.materialType;
	var number_people = req.body.number_people;
	var heating = req.body.heating;
	var latitude = req.body.location.latitude;
    var longitude = req.body.location.longitude; 

	userid = sha1(userid);
	console.log(userid);
	var ref = new Firebase("https://lutgreencity.firebaseio.com/users/"+userid);
	console.log("sent");

	ref.set({});
	console.log( {
	  userid: userid,
	  username: username,
	  energy_consumption: 0,
	  energy_readings: {  
	  	value: 0
	  },
	  profile: {
	  	material: material,
	  	number_people: number_people,
	  	heating: heating
	  },
	  location: {
        latitude: latitude,
        longitude: longitude 
      }
	} );
  	var newRef = ref.set(  {
	  userid: userid,
	  username: username,
	  energy_consumption: 0,
	  energy_readings: {  
	  	value: 0
	  },
	  profile: {
	  	material: material,
	  	number_people: number_people,
	  	heating: heating
	  },
	  location: {
        latitude: latitude,
        longitude: longitude 
      }
	} , function() { res.json({"response": "Successfully!"}); });
	
});


router.get('/createUser/:userid/:username', function(req, res, next) {
	var userid = req.params.userid;
	var username = req.params.username;
	userid = sha1(userid);
	console.log(userid);
	var ref = new Firebase("https://lutgreencity.firebaseio.com/users/"+userid);
  	var newRef = ref.set({
	  userid: userid,
	  username: username,
	  energy_consumption: 0,
	  energy_readings: {  
	  	value: 0
	  },
	  location: {
        latitude: 61.047878,
        longitude: 28.11139 
      },
	});
	console.log("sent");
	res.json({"response": "Success!"});
});



module.exports = router;
